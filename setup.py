#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
    name='Reflector',
    version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(  1625773972)),
    description='''A Python 3 module and script to retrieve and filter the latest Pacman mirror list.''',
    author='Xyne',
    author_email='ac xunilhcra enyx, backwards',
    url='''http://xyne.archlinux.ca/projects/reflector''',
    py_modules=['Reflector'],
    scripts=['reflector']
)
