# Name

reflector - retrieve and filter the latest Pacman mirrorlist

# Synopsis

`reflector [arguments]`


# Description

reflector is a Python script and associated Python module that can retrieve up-to-date Arch Linux mirror data from the [Mirror Status](https://www.archlinux.org/mirrors/status/) web interface.


# Arguments

See `reflector --help`.

# Examples

Print the latest mirrorlist to STDOUT:

~~~{.sh}
reflector
~~~

Sort the five most recently synchronized mirrors by download speed and overwrite the local mirrorlist:

~~~{.sh}
reflector --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
~~~

Select the 200 most recently synchronized HTTP or HTTPS mirrors, sort them by download speed, and overwrite the file /etc/pacman.d/mirrorlist:

~~~{.sh}
reflector --latest 200 --protocol http,https --sort rate --save /etc/pacman.d/mirrorlist
~~~

Select  the HTTPS mirrors synchronized within the last 12 hours and located in either France or Germany, sort them by download speed, and overwrite the file `/etc/pacman.d/mirrorlist` with the results:

~~~{.sh}
reflector --country France,Germany --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
~~~


# Systemd Integration

Reflector includes systemd service and timer units that can be used to automatically update Pacman's mirrorlist. To use the timer, edit the configuration file at `/etc/xdg/reflector/reflector.conf` and then enable the timer with

~~~{.sh}
systemctl enable reflector.timer
systemctl start reflector.timer
~~~

Check that status with `systemctl list-timers`. To update the mirrorlist immediately instead of waiting for the scheduled operation, run `systemctl start reflector.service`.

# See Also

`pacman(8)`
